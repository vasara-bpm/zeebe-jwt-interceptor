package org.camunda.community.zeebe.jwt.interceptor;

import io.grpc.*;
import io.jsonwebtoken.*;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.List;

import static io.grpc.Metadata.ASCII_STRING_MARSHALLER;

final class Constant {
    static final String SIGNING_KEY =
            System.getenv("JWT_INTERCEPTOR_SIGNING_KEY") != null
                    ? System.getenv("JWT_INTERCEPTOR_SIGNING_KEY")
                    : null;
    static final String SIGNING_ALG =
            System.getenv("JWT_INTERCEPTOR_SIGNING_ALG") != null
                    ? System.getenv("JWT_INTERCEPTOR_SIGNING_ALG")
                    : "RSA";
    static final String AUDIENCE =
            System.getenv("JWT_INTERCEPTOR_AUDIENCE") != null
                    ? System.getenv("JWT_INTERCEPTOR_AUDIENCE")
                    : "zeebe";
    static final String AUTHORIZED_TENANTS_CLAIM =
            System.getenv("JWT_INTERCEPTOR_AUTHORIZED_TENANTS_CLAIM") != null
                    ? System.getenv("JWT_INTERCEPTOR_AUTHORIZED_TENANTS_CLAIM")
                    : "authorized_tenants";
    static final Metadata.Key<String> AUTHORIZATION_METADATA_KEY = Metadata.Key.of("Authorization", ASCII_STRING_MARSHALLER);
    static final Context.Key<List<String>> AUTHORIZED_TENANTS_CONTEXT_KEY = Context.key("io.camunda.zeebe:authorized_tenants");
}

public class JwtInterceptor implements ServerInterceptor {

    private final PublicKey signingKey;

    public JwtInterceptor() {
        this(Constant.SIGNING_KEY);
    }
    public JwtInterceptor(String publicKeyPEM) {
        byte[] keyBytes = java.util.Base64.getDecoder().decode(publicKeyPEM);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = null;
        try {
            keyFactory = KeyFactory.getInstance(Constant.SIGNING_ALG);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        try {
            signingKey = keyFactory.generatePublic(keySpec);
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> serverCall,
                                                                 Metadata metadata, ServerCallHandler<ReqT, RespT> serverCallHandler) {
        JwtParser parser = Jwts.parser().setSigningKey(signingKey);
        String value = metadata.get(Constant.AUTHORIZATION_METADATA_KEY);
        Status status = Status.OK;
        if (value == null) {
            status = Status.UNAUTHENTICATED.withDescription("Authorization token is missing");
        } else if (!value.startsWith("Bearer ")) {
            status = Status.UNAUTHENTICATED.withDescription("Unknown authorization type");
        } else {
            Jws<Claims> claims = null;
            String token = value.substring("Bearer ".length()).trim();
            try {
                // Verify token and pares claims
                claims = parser.parseClaimsJws(token);
            } catch (JwtException e) {
                status = Status.UNAUTHENTICATED.withDescription(e.getMessage()).withCause(e);
            }
            if (claims != null) {
                try {
                    List<String> authorizedTenants = (List<String>) claims.getBody().get(Constant.AUTHORIZED_TENANTS_CLAIM);
                    if (authorizedTenants != null) {
                        Context ctx = Context.current()
                                .withValue(Constant.AUTHORIZED_TENANTS_CONTEXT_KEY, authorizedTenants);
                        return Contexts.interceptCall(ctx, serverCall, metadata, serverCallHandler);
                    }
                } catch (ClassCastException e) {
                    // noop
                }
            }
        }
        serverCall.close(status, new Metadata());
        return new ServerCall.Listener<ReqT>() {
            // noop
        };
    }
}
