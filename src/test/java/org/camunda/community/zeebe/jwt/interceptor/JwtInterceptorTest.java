package org.camunda.community.zeebe.jwt.interceptor;

import static org.junit.Assert.*;
import static org.mockito.AdditionalAnswers.delegatesTo;
import static org.mockito.Mockito.*;

import io.grpc.*;
import io.grpc.ForwardingClientCall.SimpleForwardingClientCall;
import io.grpc.examples.helloworld.GreeterGrpc;
import io.grpc.examples.helloworld.GreeterGrpc.GreeterBlockingStub;
import io.grpc.examples.helloworld.GreeterGrpc.GreeterImplBase;
import io.grpc.examples.helloworld.HelloReply;
import io.grpc.examples.helloworld.HelloRequest;
import io.grpc.inprocess.InProcessChannelBuilder;
import io.grpc.inprocess.InProcessServerBuilder;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import io.grpc.testing.GrpcCleanupRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.ArgumentCaptor;

@RunWith(JUnit4.class)
public class JwtInterceptorTest {
    /**
     * This rule manages automatic graceful shutdown for the registered servers and channels at the
     * end of test.
     */
    @Rule
    public final GrpcCleanupRule grpcCleanup = new GrpcCleanupRule();

    private Channel channel;

    private static final String testPublicKeyPEM = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyVGmRxTtbGwQh6lmZtBwnNpbTI4+4GD52TOirSofeA10knl5BpTHpGuZgcYpyP3+DA/pj0k5Gb0sHVgpdoc3d+tMXegqGOYZAwuo5XfseOGvrsM8uZQBHN4FlZv5f6oPLyv+ttePVtKHFijU0boa3kCvpC6aCeRllYYTvf1yJha8wRwJFu+oTYOsG4rYurs2r2yWFXLGIitPq1PGpTWqvZhNe2YjYfr8JYiFv1GQ/PTXUzCM6JYXMpk9WmAphOOYozNvAdv8bWikotNVHBolteQEJXbyajIK5AmJSnm26AFpFZh4a1DewolSeMmhkhO0YsBymYB9XEkQlXY6SmaySwIDAQAB";

    private static final String expiredToken = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICI5akw0X05VdXd5T2VVcnB3ZFRCV05BZ1RPTnFhQXBBbzY2cUVmcWFkUlVFIn0.eyJleHAiOjE3MDg2MzU5MzIsImlhdCI6MTcwODYzNTkzMSwianRpIjoiZTI3MWMxN2UtOTEwZi00MzAzLWFhOTAtYjA1ZTQ0ODE3OTI4IiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgwL3JlYWxtcy96ZWViZSIsImF1ZCI6ImFjY291bnQiLCJzdWIiOiIxYWM4MzUyZi1jOGRkLTQxMzAtYThmMS01ZTMwOWJhZmQ4YTgiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJ6ZWViZSIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiLyoiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImRlZmF1bHQtcm9sZXMtemVlYmUiLCJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgcHJvZmlsZSBlbWFpbCIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiY2xpZW50SG9zdCI6IjEwLjAuMi4xMDAiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJzZXJ2aWNlLWFjY291bnQtemVlYmUiLCJjbGllbnRBZGRyZXNzIjoiMTAuMC4yLjEwMCIsImNsaWVudF9pZCI6InplZWJlIn0.KXrXW72_FFMWN_77KaVvITrmw2LPZyMjJKN4LiVLXZUzJHnY794kia4Gwvi-S1lqvGCzpSHSlL8HkAwWFDKGzdyC9v9vvoNEd2IhXONaDvIyeDopZaWxqLqzQDDS_3XkxkWVt3Iaj3NeDthQkkYqnGffchvBpu54XHIPEequ1VyaLe1g-_oSz7cf6K7G39grfjqI2aXx82ct-le8ZzBf3KR6Q-fXICKXUVcI8jfobucTjgNcRAX9QLO3a20x2q44-JTFkEzQ4jOsvvusjTU68BgsYB0H8Fy9HNWbnDTY5YtBVYhXdgSQ2sCTJGEfxb42L9vyGk3fqqW41iJwprGurQ";

    private static final String withoutTenants = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICI5akw0X05VdXd5T2VVcnB3ZFRCV05BZ1RPTnFhQXBBbzY2cUVmcWFkUlVFIn0.eyJleHAiOjE3MDg2NzI3NjUsImlhdCI6MTcwODYzNjc2NSwianRpIjoiYTkzNWExMTgtNWRmMS00ZTlmLWJiZjQtMTNkYTUxNzNmYTRmIiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgwL3JlYWxtcy96ZWViZSIsImF1ZCI6ImFjY291bnQiLCJzdWIiOiIxYWM4MzUyZi1jOGRkLTQxMzAtYThmMS01ZTMwOWJhZmQ4YTgiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJ6ZWViZSIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiLyoiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImRlZmF1bHQtcm9sZXMtemVlYmUiLCJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgcHJvZmlsZSBlbWFpbCIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiY2xpZW50SG9zdCI6IjEwLjAuMi4xMDAiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJzZXJ2aWNlLWFjY291bnQtemVlYmUiLCJjbGllbnRBZGRyZXNzIjoiMTAuMC4yLjEwMCIsImNsaWVudF9pZCI6InplZWJlIn0.lAEEV02nnFTRWgmFPGPKqlsZt-fSb0tj5UpJZPnFvMVX4ZJIDDP2lSQsPGDta04WPW3Fg9mbRInt9_rc3q6yBqoH4m2s92zRzMdWbBQ4SbgojAihuu2Cf8borEms9dbesh8EusJ9FAc1GzbB0Ff7Nmts2pjWHh0kpuAYTED1DE9z8niukGQ_DqOgxAsh9zcRllORT_9ulSFfpGRWIob8CeO07tl-ngIz94v9apgSjsVgyyVTFoB55IqvuSxonXDYQuDDrhhbLwlwLZNy1XOCn50sBYwq96LczSnQnxhm1yyORGxL-ZdAdpI_hr_EjRiA9kk1ElYZX9krynCxrhoumg";

    private static final String withTenants = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICI5akw0X05VdXd5T2VVcnB3ZFRCV05BZ1RPTnFhQXBBbzY2cUVmcWFkUlVFIn0.eyJleHAiOjE3MDg2NzMyMzksImlhdCI6MTcwODYzNzIzOSwianRpIjoiMjA0Y2ViYTEtOWFmNi00MTY4LWEyMTAtZWIzNTVjNTQzZTE1IiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgwL3JlYWxtcy96ZWViZSIsImF1ZCI6ImFjY291bnQiLCJzdWIiOiIxYWM4MzUyZi1jOGRkLTQxMzAtYThmMS01ZTMwOWJhZmQ4YTgiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJ6ZWViZSIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiLyoiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImRlZmF1bHQtcm9sZXMtemVlYmUiLCJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgYXV0aG9yaXplZF90ZW5hbnRzIHByb2ZpbGUgZW1haWwiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImNsaWVudEhvc3QiOiIxMC4wLjIuMTAwIiwicHJlZmVycmVkX3VzZXJuYW1lIjoic2VydmljZS1hY2NvdW50LXplZWJlIiwiYXV0aG9yaXplZF90ZW5hbnRzIjpbImFsZmEiLCJiZXRhIl0sImNsaWVudEFkZHJlc3MiOiIxMC4wLjIuMTAwIiwiY2xpZW50X2lkIjoiemVlYmUifQ.NVee1yQL6bdoxo-qP82WxjvRlWdPMwLvGfqlLnHForHXUNwldUWkYn9tqS8oy8yb6IBAuSz6aRs5VQFg0Z6-ll5XiKlaP03UTi3c6M2Q9egGV147KzWtv5YDoDUtfi3OpaU_-I9VEo9wcCA0upQ-hKqfV0TCoDD0dtYIiyVtSJ36qPiLFXD9QtraqcAcm7iuF6cqqHmr7NjGd2bMqnWryNXBAmOsXEeIYP5_Dit-sL0rFMclw4PF35GhaULoK36fxPNXmmzF_M1i0nAhAR85Ucx47oC8CriI9hziKYHYyUWCoUlC-p-NIs2aUX7QtEhuPwIlIk-_LLZoeDkkLUh52A";

    @Before
    public void setUp() throws Exception {
        GreeterImplBase greeterImplBase =
                new GreeterImplBase() {
                    @Override
                    public void sayHello(HelloRequest request, StreamObserver<HelloReply> responseObserver) {
                        responseObserver.onNext(HelloReply.getDefaultInstance());
                        responseObserver.onCompleted();
                    }
                };
        // Generate a unique in-process server name.
        String serverName = InProcessServerBuilder.generateName();
        // Create a server, add service, start, and register for automatic graceful shutdown.
        grpcCleanup.register(InProcessServerBuilder.forName(serverName).directExecutor()
                .addService(ServerInterceptors.intercept(greeterImplBase, new JwtInterceptor(testPublicKeyPEM)))
                .build().start());
        // Create a client channel and register for automatic graceful shutdown.
        channel =
                grpcCleanup.register(InProcessChannelBuilder.forName(serverName).directExecutor().build());
    }

    @Test
    public void testWithoutTokenUnauthenticated() {
        GreeterBlockingStub blockingStub = GreeterGrpc.newBlockingStub(channel);

        StatusRuntimeException exception = assertThrows(StatusRuntimeException.class, () -> {
            blockingStub.sayHello(HelloRequest.getDefaultInstance());
        });

        assertEquals(exception.getMessage(),"UNAUTHENTICATED: Authorization token is missing");
    }

    @Test
    public void testExpiredToken() {
        Metadata metadata = new Metadata();
        metadata.put(Metadata.Key.of("Authorization", Metadata.ASCII_STRING_MARSHALLER), "Bearer " + expiredToken);

        GreeterBlockingStub blockingStub = GreeterGrpc.newBlockingStub(channel)
                .withInterceptors(MetadataUtils.newAttachHeadersInterceptor(metadata));

        StatusRuntimeException exception = assertThrows(StatusRuntimeException.class, () -> {
            blockingStub.sayHello(HelloRequest.getDefaultInstance());
        });

        assertTrue(exception.getMessage().startsWith("UNAUTHENTICATED: JWT expired"));
    }

    @Test
    public void testTokenWithoutTenants() {
        Metadata metadata = new Metadata();
        metadata.put(Metadata.Key.of("Authorization", Metadata.ASCII_STRING_MARSHALLER), "Bearer " + withoutTenants);

        GreeterBlockingStub blockingStub = GreeterGrpc.newBlockingStub(channel)
                .withInterceptors(MetadataUtils.newAttachHeadersInterceptor(metadata));

        StatusRuntimeException exception = assertThrows(StatusRuntimeException.class, () -> {
            blockingStub.sayHello(HelloRequest.getDefaultInstance());
        });

        assertEquals(exception.getMessage(),"CANCELLED: server cancelled stream");
    }

    /*
    @Test
    public void testValidToken() {
        Metadata metadata = new Metadata();
        metadata.put(Metadata.Key.of("Authorization", Metadata.ASCII_STRING_MARSHALLER), "Bearer " + withTenants);

        GreeterBlockingStub blockingStub = GreeterGrpc.newBlockingStub(channel)
                .withInterceptors(MetadataUtils.newAttachHeadersInterceptor(metadata));

        blockingStub.sayHello(HelloRequest.getDefaultInstance());

        assertTrue(true);
    }
    */

    @Test
    public void testInterceptorModifiesContext() {
        // Create a mock ServerCall and ServerCallHandler
        ServerCall<Object, Object> mockServerCall = mock(ServerCall.class);
        ServerCallHandler<Object, Object> mockServerCallHandler = mock(ServerCallHandler.class);

        // Create an instance of your interceptor
        JwtInterceptor interceptor = new JwtInterceptor(testPublicKeyPEM);

        // Set up a metadata object with any required headers
        Metadata headers = new Metadata();
        headers.put(Metadata.Key.of("Authorization", Metadata.ASCII_STRING_MARSHALLER), "Bearer " + withoutTenants);

        // Create a mock ServerCall.Listener to handle the response
        ServerCall.Listener<Object> mockListener = mock(ServerCall.Listener.class);

        // Invoke the interceptor's interceptCall method
        ServerCall.Listener<Object> resultListener = interceptor.interceptCall(
                mockServerCall,
                headers,
                mockServerCallHandler
        );

        // Ensure the context has been modified as expected
        // You might need to customize this part based on how your interceptor modifies the context
        //assertNotNull(Context.current().get("custom-key"), "Context should be modified");

        // Continue the call with the mock listener
        resultListener.onReady();

        // Verify that the ServerCallHandler's startCall method was called with the expected parameters
//        verify(mockServerCallHandler).startCall(
 //               eq(mockServerCall),
  //              eq(headers),
   //             any(Metadata.class),
    //            eq(mockListener)
     //   );
    }
}
